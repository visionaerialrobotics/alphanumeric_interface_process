/*!*******************************************************************************************
 *  \copyright Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#ifndef DRONEMODULEINTERFACE_H
#define DRONEMODULEINTERFACE_H

// ROS
#include "ros/ros.h"
#include "std_srvs/Empty.h"
#include "std_msgs/Bool.h"
#include "droneMsgsROS/askForModule.h"

// C++ standar libraries
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <sstream>

// Topic and Rosservice names
#include "communication_definition.h"

#define DRONE_MODULE_INTERFACE_ISSTARTED_TIME_THRESHOLD (2.0)

class DroneModuleInterface
{
  // Ros node
protected:
  ros::NodeHandle n;
  std::string module_name_str;
  ModuleNames::name module_name_enum;

private:
  std_srvs::Empty emptySrv;
  ros::ServiceClient moduleIsStartedClientSrv;
  ros::ServiceClient moduleIsOnlineClientSrv;
  ros::ServiceClient resetClientSrv;
  ros::ServiceClient startClientSrv;
  ros::ServiceClient stopClientSrv;
  // ros::Subscriber    isStartedSub;
  bool moduleStarted;
  bool moduleOnline;
  ros::Time last_isStarted_timestamp;
  // void isStartedSubCallback(const std_msgs::Bool::ConstPtr &msg);
  // void checkStarted();

public:
  DroneModuleInterface(const std::string& module_name_str_in, const ModuleNames::name module_name_enum_in);
  DroneModuleInterface(const std::string& module_name_str_in);
  ~DroneModuleInterface();
  void open(ros::NodeHandle& nIn);

  bool start(bool block_execution_until_state_estimator_is_started = false);
  bool stop();
  bool reset();
  int isStarted();
  int isOnline();
  std::string getModuleName()
  {
    return module_name_str;
  }
};

#endif  // DRONEMODULEINTERFACE_H
